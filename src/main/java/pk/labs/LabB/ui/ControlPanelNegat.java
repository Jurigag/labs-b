package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.ControlPanel;

/**
 * Created by Wojtek on 2015-01-07.
 */
@Component("cpneg")
public class ControlPanelNegat implements Negativeable{
    Utils utils;

    public ControlPanelNegat()
    {
        this.utils = new Utils();
    }

    @Override
    public void negative() {
        utils.negateComponent(((ControlPanel) AopContext.currentProxy()).getPanel());
    }
}
