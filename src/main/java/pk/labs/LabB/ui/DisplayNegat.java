package pk.labs.LabB.ui;

import org.springframework.aop.framework.AopContext;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Display;

/**
 * Created by Wojtek on 2015-01-07.
 */
@Component("dpneg")
public class DisplayNegat implements Negativeable {
    Utils utils;

    public DisplayNegat()
    {
        this.utils = new Utils();
    }
    @Override
    public void negative() {
        utils.negateComponent(((Display)AopContext.currentProxy()).getPanel());
    }
}
