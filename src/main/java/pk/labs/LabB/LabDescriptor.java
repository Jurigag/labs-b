package pk.labs.LabB;

import pk.labs.LabB.Contracts.UkladWejsciowy;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = "pk.labs.LabB.DisplayImpl";
    public static String controlPanelImplClassName = "pk.labs.LabB.ControlPanelImpl";

    public static String mainComponentSpecClassName = UkladWejsciowy.class.getName();
    public static String mainComponentImplClassName = "pk.labs.LabB.UkladWejsciowyImpl";
    public static String mainComponentBeanName = "UkladWyjsciowy";
    // endregion

    // region P2
    public static String mainComponentMethodName = "Dzialam";
    public static Object[] mainComponentMethodExampleParams = new Object[] { };
    // endregion

    // region P3
    public static String loggerAspectBeanName = "logger";
    // endregion
}
