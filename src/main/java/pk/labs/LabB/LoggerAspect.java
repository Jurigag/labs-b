package pk.labs.LabB;

import org.aspectj.lang.JoinPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pk.labs.LabB.Contracts.Logger;

/**
 * Created by Wojtek on 2015-01-06.
 */

@Component("logger")
public class LoggerAspect {
    private Logger logger;

    public void setLogger(Logger logger)
    {
        this.logger=logger;
    }

    public Logger getLogger()
    {
        return this.logger;
    }

    @Autowired
    public LoggerAspect(Logger l)
    {
        this.logger=l;
    }

    public void logMethodEx(JoinPoint j, Object r)
    {
        if (this.logger!=null)
        this.logger.logMethodExit(j.getSignature().getName(),r);
    }

    public void logMethodEn(JoinPoint j)
    {
        if (this.logger!=null)
        this.logger.logMethodEntrance(j.getSignature().getName(),j.getArgs());
    }
}
